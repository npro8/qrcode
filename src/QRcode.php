<?php
namespace npro\qrcode;

use think\Exception;
use think\facade\Config;
use think\facade\Request;

class QRcode
{
    protected $config = []; // 相关配置
    protected $cache_dir = ''; // 二维码缓存
    protected $outfile = ''; //输出二维码文件
    protected $error = ''; //错误信息
    protected $public_dir = ''; //项目跟目录

    public function __construct()
    {
        $this->config = Config::get('qrcode.');

        if (isset($this->config['cache_dir']) && $this->config['cache_dir'] != '') {
            $this->cache_dir = $this->config['cache_dir'];
        } else {
            $this->cache_dir = 'uploads/qrcode' . date("Ymd");
        }

        $this->public_dir = public_path() . "/";

        if (!file_exists($this->cache_dir)) @mkdir($this->public_dir . $this->cache_dir, 0775, true);

        require("phpqrcode/qrlib.php");
    }

    /**
     * 生成普通二维码
     * @param string $url  生成url地址
     * @param bool $outfile
     * @param int $size
     * @param string $evel
     * @return $this
     */
    public function png($url, $outfile = false, $size = 5, $evel = 'H')
    {
        if (!$outfile) $outfile = $this->cache_dir . '/' . time() . '.png';
        $this->outfile = $outfile;
        \QRcode::png($url, $this->public_dir . $outfile, $evel, $size, 2);
        return $this;
    }

    /**
     * 显示二维码
     */
    public function show()
    {
        $url = Request::instance()->domain() . '/' . $this->outfile;
        exit('<img src="'.$url.'" />');
    }

    /**
     * 返回url路径
     * @return string
     */
    public function entry()
    {
        return Request::instance()->domain() . '/' . $this->outfile;
    }

    /**
     * 返回生成二维码的相对路径
     * @param bool $ds
     * @return string
     */
    public function getPath($ds = true)
    {
        return $ds ? "/{$this->outfile}" : $this->outfile;
    }

    /**
     * 销毁内容
     */
    public function destroy()
    {
        @unlink($this->outfile);
    }

    /**
     * 添加logo到二维码中
     * @param $logo
     * @return bool|mixed
     */
    public function logo($logo, $new_name = false)
    {
        if (!isset($logo) || empty($logo)) {
            $this->error = 'logo不存在';
            return false;
        }
        $QR = imagecreatefromstring(file_get_contents($this->public_dir . $this->outfile));
        $logo = imagecreatefromstring($logo);
        $QR_width = imagesx($QR);//二维码图片宽度
        $QR_height = imagesy($QR);//二维码图片高度
        $logo_width = imagesx($logo);//logo图片宽度
        $logo_height = imagesy($logo);//logo图片高度
        $logo_qr_width = bcdiv($QR_width, 5);
        $scale = bcdiv($logo_width, $logo_qr_width);
        $logo_qr_height = bcdiv($logo_height, $scale);
        $from_width = bcdiv(($QR_width - $logo_qr_width), 2);
        //重新组合图片并调整大小
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

        if ($new_name) $this->outfile = $new_name;
        imagepng($QR, $this->public_dir . $this->outfile);
        imagedestroy($QR);
        return $this;
    }

    /**
     * 添加背景图
     * @param int $x 二维码在背景图X轴未知
     * @param int $y 二维码在背景图Y轴未知
     * @param string $dst_path
     * @return $this
     */
    public function background($x = 200, $y = 500, $dst_path = '', $new_name = false)
    {
        if (empty($dst_path)) $dst_path = $this->config['background'] ?? '';
        if (empty($dst_path)) throw new \Exception('错误的背景图片');
        $src_path = $this->public_dir . $this->outfile; // 覆盖图

        // 创建背景图的实例
        $dst = imagecreatefromstring(file_get_contents($this->public_dir . $dst_path));
        if ($dst === false) throw new \Exception('无法创建背景图片');

        // 创建覆盖图的实例
        $src = imagecreatefromstring(file_get_contents($src_path));
        if ($src === false) throw new \Exception('无法创建覆盖图片');

        // 确保背景图支持透明度
        imagealphablending($dst, false);
        imagesavealpha($dst, true);

        // 确保覆盖图支持透明度
        imagealphablending($src, false);
        imagesavealpha($src, true);

        // 获取覆盖图图片的宽高
        list($src_w, $src_h) = getimagesize($src_path);

        // 将覆盖图复制到目标图片上，保留透明度
        imagecopy($dst, $src, $x, $y, 0, 0, $src_w, $src_h);

        if ($new_name) $this->outfile = $new_name;

        // 保存图像时设置透明度
        imagepng($dst, $this->public_dir . $this->outfile); // 根据需要生成相应的图片

        // 释放内存
        imagedestroy($dst);
        imagedestroy($src);

        return $this;
    }

    /**
     * 添加文字内容
     * @param string $text 内容
     * @param int $size 字体大小
     * @param array $locate 位置信息
     * @param string $color 颜色
     * @param string $font 字体
     * @param int $angle 斜角角度
     * @return $this
     */
    public function text($text, $size, $locate = [], $color = '#00000000', $font='simsun.ttc', $offset = 0, $angle = 0, $new_name = false)
    {
        $dst_path = $this->public_dir . $this->outfile;

        //创建图片的实例
        $dst = imagecreatefromstring(file_get_contents($dst_path));

        // 确保背景图支持透明度
        imagealphablending($dst, false);
        imagesavealpha($dst, true);

        /* 设置颜色 */
        if (is_string($color) && 0 === strpos($color, '#')) {
            $color = str_split(substr($color, 1), 2);
            $color = array_map('hexdec', $color);
            if (empty($color[3]) || $color[3] > 127) {
                $color[3] = 0;
            }
        } elseif (!is_array($color)) {
            throw new Exception('错误的颜色值');
        }

        //如果字体不存在 用composer项目自己的字体
        if(!is_file($font)) $font =  dirname(__FILE__) . '/' . $font;

        //获取文字信息
        $info = imagettfbbox($size, $angle, $font, $text);
        $minx = min($info[0], $info[2], $info[4], $info[6]);
        $maxx = max($info[0], $info[2], $info[4], $info[6]);
        $miny = min($info[1], $info[3], $info[5], $info[7]);
        $maxy = max($info[1], $info[3], $info[5], $info[7]);
        /* 计算文字初始坐标和尺寸 */
        $x = $minx;
        $y = abs($miny);
        $w = $maxx - $minx;
        $h = $maxy - $miny;

        //背景图信息
        list($dst_w, $dst_h) = getimagesize($dst_path);

        if (is_array($locate)) {
            list($posx, $posy) = $locate;
            $x += ($posx=='center')?(bcdiv(($dst_w - $w), 2)):$posx;
            $y += ($posy=='center')?(bcdiv(($dst_h - $h), 2)):$posy;
        } else {
            throw new Exception('不支持的文字位置类型');
        }

        //字体颜色
        $black = imagecolorallocate($dst,  $color[0], $color[1], $color[2]);

        //加入文字
        imagefttext($dst, $size, $angle, $x, $y, $black, $font, $text);

        //生成图片
        if ($new_name) $this->outfile = $new_name;
        imagepng($dst, $this->public_dir . $this->outfile);
        imagedestroy($dst);

        return $this;
    }
}